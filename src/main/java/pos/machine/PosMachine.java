package pos.machine;

import java.util.List;
import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        if(barcodesIsNotEmpty(barcodes)) {
           return generateReceipt(generateBarcodeCountMap(barcodes));
        }
        return null;
    }

    //  Check whether the input is an empty list
    public boolean barcodesIsNotEmpty(List<String> barcodes) {
        return barcodes.size() != 0;
    };
    // Generates a map of barcodes and quantities
    public LinkedHashMap<String,Integer> generateBarcodeCountMap(List<String> barcodes) {
        LinkedHashMap<String,Integer> map=new LinkedHashMap<String,Integer>();
        for(String barcode:barcodes)
        {
            if(map.containsKey(barcode))
            {
                int val=map.get(barcode);
                val++;
                map.put(barcode, val);
            }
            else
            {
                map.put(barcode, 1);
            }
        }
        return map;
    };

     public String generateSingleCommodityMessage(String barcode,int quantity){
        List<Item> items = ItemsLoader.loadAllItems();
        String itemString = "";
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getBarcode() == barcode) {
               itemString = "Name: " + items.get(i).getName() + ", Quantity: "+quantity+", Unit price: "+items.get(i).getPrice() +" (yuan), Subtotal: "+items.get(i).getPrice() * quantity+" (yuan)";
            }
        }
        return itemString;
    }

    public int calculateReceiptTotalPrice(LinkedHashMap<String,Integer> barcodesCountMap){
        List<Item> items = ItemsLoader.loadAllItems();
        int totalPrice = 0;
        for(String barcode:barcodesCountMap.keySet())
        {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getBarcode() == barcode) {
                    totalPrice += items.get(i).getPrice() * barcodesCountMap.get(barcode);
                }
            }
        }
        return totalPrice;
    }

     public String generateReceipt(LinkedHashMap<String,Integer> barcodesCountMap){
        int totalPrice = 0;
        totalPrice = calculateReceiptTotalPrice(barcodesCountMap);
        if (totalPrice == 0) {
            return null;
        }
        String receiptString = "***<store earning no money>Receipt***\n";
        for(String barcode:barcodesCountMap.keySet())
        {
           receiptString += generateSingleCommodityMessage(barcode,barcodesCountMap.get(barcode))+'\n';
        }
        receiptString += "----------------------\n";

        receiptString += "Total: "+totalPrice+" (yuan)\n";
        receiptString += "**********************";
        return receiptString;
    }
}
