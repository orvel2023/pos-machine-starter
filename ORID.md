# O
  - Tasking : Decompose task down into smaller tasks so they are manageable and verifiable.
  - Context Map : Learned how to clarify the development process and do a good job of code specification.
  - Development exercise : Let myself deepen the understanding of the standardized development process, deepen the knowledge of the above.

# R
  - Challenging !
# I
  - I think today's class has changed my usual programming habits.The biggest significance is that can make my future development more standardized and efficient.
# D
  - Consciously use tasking in development or daily work to solve problems more effectively.
  - Use context map to make the development process more explicit and clear.
  - Learn the Java language in depth.